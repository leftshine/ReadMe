package cn.leftshine.readme.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.provider.Settings;

import cn.leftshine.readme.MyApplication;

/**
 * Created by Administrator on 2018/10/24.
 */

public class SettingsHelper {
    private final String NIGHT_MODE = "night_mode";
    private final String LAST_THEME_FILE_NAME = "last_theme_file_name";

    private SharedPreferences sp;
    private SharedPreferences.Editor editor;

    public SettingsHelper(Context context){
        sp = PreferenceManager.getDefaultSharedPreferences(context);
        editor = sp.edit();
    }
    public SettingsHelper(){
        sp = PreferenceManager.getDefaultSharedPreferences(MyApplication.getContext());
        editor = sp.edit();
    }

    private void setBoolean(String key, boolean value) {
        editor.putBoolean(key,value);
        editor.commit();
    }

    private boolean getBoolean(String key) {
        return sp.getBoolean(key,false);
    }

    private void setString(String key, String value) {
        editor.putString(key,value);
        editor.commit();
    }

    private String getString(String key,String defValue) {
        return sp.getString(key,defValue);
    }

    public boolean isNightMode() {
        return getBoolean(NIGHT_MODE);
    }

    public void setNightmode(boolean nightMode) {
        setBoolean(NIGHT_MODE,nightMode);
    }

    public String getLastThemeFilename() {
        return getString(LAST_THEME_FILE_NAME,ThemeInfoHelper.DEFAULT_SKIN_FILE_NAME);
    }

    public void setLastThemeFilename(String ThemeFilename) {
       setString(LAST_THEME_FILE_NAME,ThemeFilename);
    }
}
