package cn.leftshine.readme;

import android.content.Context;

import cn.leftshine.readme.theme_attr.BottomNavigationAttr;
import solid.ren.skinlibrary.SkinConfig;
import solid.ren.skinlibrary.base.SkinBaseApplication;

/**
 * Created by Administrator on 2018/10/19.
 */

public class MyApplication extends SkinBaseApplication {

    private static MyApplication mInstance;
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        context = getApplicationContext();
        //ToastUtils.init(this);

        SkinConfig.setCanChangeStatusColor(true);
        SkinConfig.addSupportAttr("bottomNavigationTint", new BottomNavigationAttr());
    }

    public static MyApplication getInstance() {
        return mInstance;
    }

    /**
     * 获取全局上下文*/
    public static Context getContext() {
        return context;
    }
}
