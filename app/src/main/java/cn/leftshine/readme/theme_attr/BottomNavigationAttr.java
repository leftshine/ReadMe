package cn.leftshine.readme.theme_attr;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.util.Log;
import android.view.View;

import cn.leftshine.readme.helper.SettingsHelper;
import solid.ren.skinlibrary.attr.base.SkinAttr;
import solid.ren.skinlibrary.loader.SkinManager;
import solid.ren.skinlibrary.utils.SkinResourcesUtils;

/**
 * Created by Administrator on 2018/10/22.
 */

public class BottomNavigationAttr extends SkinAttr {
    @Override
    public void applySkin(View view) {
        if (view instanceof BottomNavigationView) {
            Log.i("BottomNavigationAttr", "apply");
            BottomNavigationView bnv = (BottomNavigationView) view;
            if (RES_TYPE_NAME_COLOR.equals(attrValueTypeName)) {
                Log.i("BottomNavigationAttr", "apply color");
                int color = SkinResourcesUtils.getColor(attrValueRefId);
                bnv.setItemTextColor(createSelector(color));
                bnv.setItemIconTintList(createSelector(color));
            } else if (RES_TYPE_NAME_DRAWABLE.equals(attrValueTypeName)) {
                Log.i("BottomNavigationAttr", "apply drawable");
                //  tv.setDivider(SkinManager.getInstance().getDrawable(attrValueRefId));
            }
        }
    }

    private ColorStateList createSelector(int color) {
        SettingsHelper settingsHelper = new SettingsHelper();
        int statePressed = android.R.attr.state_checked;
        int stateChecked = android.R.attr.state_checked;
        int[][] state = {{statePressed}, {-statePressed}, {stateChecked}, {-stateChecked}};
        int color1 = color;
        int color2 = Color.parseColor("#AAAAAA");
        int color3 = color;
        int color4 = Color.parseColor("#AAAAAA");
        if (settingsHelper.isNightMode()) {
            color2 = Color.parseColor("#424242");
            color4 = Color.parseColor("#424242");
        }
        int[] colors = {color1, color2, color3, color4};
        ColorStateList colorStateList = new ColorStateList(state, colors);
        return colorStateList;
    }
}
