package cn.leftshine.readme.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import cn.leftshine.readme.R;
import cn.leftshine.readme.beans.ThemeItem;

/**
 * Created by Administrator on 2018/10/26.
 */

public abstract class ThemeGridViewAdpter extends BaseAdapter {
    private ArrayList<ThemeItem> items = null;
    private int mLayoutRes;           //布局id
    private Context context;

    public ThemeGridViewAdpter(ArrayList<ThemeItem> themeItems,int mLayoutRes,Context context) {
        this.items = themeItems;
        this.mLayoutRes = mLayoutRes;
        this.context = context;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public ThemeItem getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = ViewHolder.bind(parent.getContext(), convertView, parent, mLayoutRes
                , position);
        bindView(holder, getItem(position));
        return holder.getItemView();
    }

    public abstract void bindView(ViewHolder holder, ThemeItem obj);

    //添加一个元素
    public void add(ThemeItem data) {
        if (items == null) {
            items = new ArrayList<>();
        }
        items.add(data);
        notifyDataSetChanged();
    }

    //往特定位置，添加一个元素
    public void add(int position, ThemeItem data) {
        if (items == null) {
            items = new ArrayList<>();
        }
        items.add(position, data);
        notifyDataSetChanged();
    }

    public void remove(ThemeItem data) {
        if (items != null) {
            items.remove(data);
        }
        notifyDataSetChanged();
    }

    public void remove(int position) {
        if (items != null) {
            items.remove(position);
        }
        notifyDataSetChanged();
    }

    public void clear() {
        if (items != null) {
            items.clear();
        }
        notifyDataSetChanged();
    }

    public void update(ArrayList<ThemeItem> themeItems){
        this.items = themeItems;
        notifyDataSetChanged();
    }

    public static class ViewHolder {

        private SparseArray<View> mViews;   //存储ListView 的 item中的View
        private View item;                  //存放convertView
        private int position;               //游标
        private Context context;            //Context上下文

        //构造方法，完成相关初始化
        private ViewHolder(Context context, ViewGroup parent, int layoutRes) {
            mViews = new SparseArray<>();
            this.context = context;
            View convertView = LayoutInflater.from(context).inflate(layoutRes, parent, false);
            convertView.setTag(this);
            item = convertView;
        }

        //绑定ViewHolder与item
        public static ViewHolder bind(Context context, View convertView, ViewGroup parent,
                                      int layoutRes, int position) {
            ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder(context, parent, layoutRes);
            } else {
                holder = (ViewHolder) convertView.getTag();
                holder.item = convertView;
            }
            holder.position = position;
            return holder;
        }

        @SuppressWarnings("unchecked")
        public <T extends View> T getView(int id) {
            T t = (T) mViews.get(id);
            if (t == null) {
                t = (T) item.findViewById(id);
                mViews.put(id, t);
            }
            return t;
        }


        /**
         * 获取当前条目
         */
        public View getItemView() {
            return item;
        }

        /**
         * 获取条目位置
         */
        public int getItemPosition() {
            return position;
        }

        /**
         * 设置文字
         */
        public ViewHolder setText(int id, CharSequence text) {
            View view = getView(id);
            if (view instanceof TextView) {
                ((TextView) view).setText(text);
            }
            return this;
        }

        /**
         * 设置背景
         */
        public ViewHolder setBackground(int id, int color) {
            View view = getView(id);
            view.setBackgroundColor(color);
            return this;
        }


        /**
         * 设置点击监听
         */
        public ViewHolder setOnClickListener(int id, View.OnClickListener listener) {
            getView(id).setOnClickListener(listener);
            return this;
        }

        /**
         * 设置可见
         */
        public ViewHolder setVisibility(int id, int visible) {
            getView(id).setVisibility(visible);
            return this;
        }

        /**
         * 设置标签
         */
        public ViewHolder setTag(int id, Object obj) {
            getView(id).setTag(obj);
            return this;
        }
    }
}
