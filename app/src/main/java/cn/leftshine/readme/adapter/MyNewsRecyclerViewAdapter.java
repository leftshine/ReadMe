package cn.leftshine.readme.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.rometools.rome.feed.synd.SyndEntry;

import org.greenrobot.eventbus.EventBus;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import cn.leftshine.readme.R;
import cn.leftshine.readme.beans.NewsItem;
import cn.leftshine.readme.event.NewsItemEvent;
import cn.leftshine.readme.view.AlignTextView;

import static cn.leftshine.readme.utils.ImgTool.getImgSrcList;

public class MyNewsRecyclerViewAdapter extends RecyclerView.Adapter<MyNewsRecyclerViewAdapter.ViewHolder> {

    private ArrayList<NewsItem> mValues;

    private Context context;

    public MyNewsRecyclerViewAdapter(ArrayList<NewsItem> items, Context context) {
        this.context = context;
        mValues = items;
    }

    public void update(ArrayList<NewsItem> items){
        mValues=items;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_news_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        SyndEntry syndEntry = holder.mItem.getSyndEntry();
        holder.news_item_title.setText(syndEntry.getTitle());
        if(null!=syndEntry.getPublishedDate()) {
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
            holder.news_item_time.setText(sdf.format(syndEntry.getPublishedDate()));
        }else{
            holder.news_item_time.setText("");
        }
        holder.news_item_comment.setText(syndEntry.getComments());
        String description = syndEntry.getDescription().getValue();
        //ArrayList<String> imgList = getImgSrcList(description);
        ArrayList<String> imgList = holder.mItem.getImgList();
        if(imgList.size()>0) {
            holder.news_item_img.setVisibility(View.VISIBLE);
            holder.news_item_time.setVisibility(View.VISIBLE);
            final String imgSrc = imgList.get(0);
            //使用Glide图片加载框架
            Glide.with(context)
                    .load(imgSrc)
                    .into(holder.news_item_img);
        }
        else{
            holder.news_item_img.setVisibility(View.GONE);
            holder.news_item_time.setVisibility(View.GONE);
        }
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //用eventBus
                EventBus.getDefault().post(new NewsItemEvent(holder.mItem));
            }
        });
    }



    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView /*news_item_title,*/news_item_time,news_item_comment;
        public final AlignTextView news_item_title;
        public final ImageView news_item_img;
        public NewsItem mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            news_item_img = view.findViewById(R.id.news_item_img);
            news_item_title = view.findViewById(R.id.news_item_title);
            news_item_time = view.findViewById(R.id.news_item_time);
            news_item_comment = view.findViewById(R.id.news_item_comment);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + news_item_title.getText() + "'";
        }

    }
}
