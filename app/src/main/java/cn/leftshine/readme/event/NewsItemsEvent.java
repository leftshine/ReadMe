package cn.leftshine.readme.event;

import com.rometools.rome.feed.synd.SyndEntry;

import java.util.ArrayList;
import java.util.List;

import cn.leftshine.readme.beans.NewsItem;

/**
 * Created by Administrator on 2018/10/23.
 */

public class NewsItemsEvent {
    private ArrayList<NewsItem> newsItems;
    private String tabTitle;

    public NewsItemsEvent(String tabTitle, ArrayList<NewsItem> newsItems){
        this.newsItems = newsItems;
        this.tabTitle = tabTitle;
    }

    public void setNewsItem(ArrayList<NewsItem> newsItems) {
        this.newsItems = newsItems;
    }

    public ArrayList<NewsItem> getNewsItem() {
        return newsItems;
    }

    public String getTabTitle() {
        return tabTitle;
    }

    public void setTabTitle(String tabTitle) {
        this.tabTitle = tabTitle;
    }
}
