package cn.leftshine.readme.event;

import com.rometools.rome.feed.synd.SyndEntry;

import cn.leftshine.readme.beans.NewsItem;

/**
 * Created by Administrator on 2018/10/23.
 */

public class NewsItemEvent {
    private NewsItem newsItem;

    public  NewsItemEvent(NewsItem newsItem){
        this.newsItem = newsItem;
    }

    public void setNewsItem(NewsItem newsItem) {
        this.newsItem = newsItem;
    }

    public NewsItem getNewsItem() {
        return newsItem;
    }

}
