package cn.leftshine.readme.fragment;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import cn.leftshine.readme.R;
import cn.leftshine.readme.adapter.NewsViewPageAdapter;
import cn.leftshine.readme.beans.NewsSource;
import cn.leftshine.readme.fragment.base.BaseFragment;
import cn.leftshine.readme.helper.DataNewsHelper;


public class ImageFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private TabLayout tabs_news;
    private ViewPager viewPager_news;
    private Toolbar toolbar;
    private AppBarLayout appbar;

    public ImageFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment NewsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ImageFragment newInstance(String param1, String param2) {
        ImageFragment fragment = new ImageFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        setHasOptionsMenu(true);
    }

    @Override
    protected int setLayoutResourceID() {
        return R.layout.fragment_image;
    }

    @Override
    protected void init() {
        super.init();
    }

    @Override
    protected void initView() {
        appbar =$(R.id.appbar);
        toolbar = $(R.id.toolbar_news);
        tabs_news  = $(R.id.tabs_news);
        viewPager_news =  $(R.id.viewPager_news);

        //toolbar.setTitle(R.string.title_news);
        //((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);

        dynamicAddView(appbar,"background",R.color.colorPrimary);

        super.initView();
    }


    @Override
    protected void initData() {
        //https://rsshub.app/jandan/pic     煎蛋无聊图
        //https://rsshub.app/pigtails   awesome pigtails
        ArrayList<NewsSource> newsDatas=new ArrayList<>();
        //newsDatas.add(new NewsSource("IT之家2",DataNewsHelper.TYPE_RSS,"https://www.ithome.com/rss/"));
        newsDatas.add(new NewsSource("ONE一个2",DataNewsHelper.TYPE_RSS_IMAGE_ARTICLE,"https://rsshub.app/one"));
        newsDatas.add(new NewsSource("煎蛋",DataNewsHelper.TYPE_RSS_IMAGE_MULTI,"https://rsshub.app/jandan/pic"));
        newsDatas.add(new NewsSource("Awesome Pigtails",DataNewsHelper.TYPE_RSS_IMAGE_MULTI,"https://rsshub.app/pigtails"));
        newsDatas.add(new NewsSource("爱范儿",DataNewsHelper.TYPE_RSS_IMAGE_MULTI," https://rsshub.app/ifanr/app"));

        //18+
        //https://rsshub.app/jandan/ooxx 煎蛋妹子
        //https://rsshub.app/mzitu/home 妹子图
        //https://rsshub.app/yande.re/post/popular_recent   yande.re
        //newsDatas.add(new NewsSource("Konachan",DataNewsHelper.TYPE_RSS,"https://rsshub.app/konachan/post/popular_recent"));




        ArrayList<String> titles=new ArrayList<>();
        ArrayList<Fragment> fragments = new ArrayList<Fragment>();
        for(NewsSource newsSource:newsDatas){
            titles.add(newsSource.tabTitle);
            //NewsListFragment newsListFragment = new NewsListFragment();
            //NewsListFragment newsListFragment = new NewsListFragment();
            ImageListFragment imageListFragment = new ImageListFragment();
            //ImageListNineFragment imageListFragment = new ImageListNineFragment();
            Bundle bundle = new Bundle();
            bundle.putString("tabTitle",newsSource.tabTitle);
            bundle.putInt("type",newsSource.type);
            bundle.putString("src",newsSource.src);
            imageListFragment.setArguments(bundle);
            fragments.add(imageListFragment);
        }
        NewsViewPageAdapter myViewPageAdapter = new NewsViewPageAdapter(getChildFragmentManager(), titles, fragments);
        viewPager_news.setAdapter(myViewPageAdapter);
        //调整fragment缓存数量，避免每次都重复加载
        viewPager_news.setOffscreenPageLimit(fragments.size()-1);
        tabs_news.setupWithViewPager(viewPager_news);
        tabs_news.setTabsFromPagerAdapter(myViewPageAdapter);

        super.initData();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_image, menu);
        /*if (menu != null) {
            if (menu.getClass() == MenuBuilder.class) {
                try {
                    Method m = menu.getClass().getDeclaredMethod("setOptionalIconsVisible", Boolean.TYPE);
                    m.setAccessible(true);
                    m.invoke(menu, true);
                } catch (Exception e) {

                }
            }
        }
        final MenuItem item = menu.findItem(R.id.action_news_calender);
        TextView tv_toolbar_today = item.getActionView().findViewById(R.id.tv_toolbar_today);
        SimpleDateFormat sdf = new SimpleDateFormat("dd");
        tv_toolbar_today.setText(sdf.format(new Date()));
        */
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if(hidden){
            //被隐藏
        }else{
            //再次显示
            ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        }
    }
}
