package cn.leftshine.readme.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import cn.leftshine.readme.R;
import cn.leftshine.readme.adapter.ImageRecyclerViewAdapter;
import cn.leftshine.readme.beans.NewsItem;
import cn.leftshine.readme.event.NewsItemsEvent;
import cn.leftshine.readme.fragment.base.BaseViewPagerFragment;
import cn.leftshine.readme.helper.DataNewsHelper;


public class ImageListFragment extends BaseViewPagerFragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";

    // TODO: Customize parameters
    private int mColumnCount = 1;
    private LinearLayout ll_main_bottom;
    private boolean isBottomShow=true;
    private String tabTitle;
    private int type;
    private String src;
    private ArrayList<NewsItem> newsItems = new ArrayList<>();
    private ImageRecyclerViewAdapter adapter;
    private SwipeRefreshLayout swipeRefreshLayout;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ImageListFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static ImageListFragment newInstance(int columnCount) {
        ImageListFragment fragment = new ImageListFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
            tabTitle = getArguments().getString("tabTitle");
            type = getArguments().getInt("type");
            src = getArguments().getString("src");

        }
    }

    @Override
    protected int setLayoutResourceID() {
        return R.layout.fragment_image_list;
    }

    @Override
    protected void init() {
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        super.init();
    }

    @Override
    protected void initView() {
        super.initView();
        View view =getContentView();

        // Set the adapter
        if (view instanceof SwipeRefreshLayout) {
            Context context = view.getContext();
            swipeRefreshLayout = (SwipeRefreshLayout) view;
            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    initData();
                }
            });
            RecyclerView recyclerView = swipeRefreshLayout.findViewById(R.id.image_list);
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }

            adapter = new ImageRecyclerViewAdapter(newsItems,getActivity());
            recyclerView.setAdapter(adapter);

            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    ll_main_bottom=getActivity().findViewById(R.id.ll_main_bottom);

                    //上滑 并且 正在显示底部栏
                    if (dy > 0 && isBottomShow) {
                        isBottomShow = false;
                        //将Y属性变为底部栏高度  (相当于隐藏了)
                        ll_main_bottom.animate().translationY(ll_main_bottom.getHeight());
                    } else if (dy < 0 && !isBottomShow) {
                        isBottomShow = true;
                        ll_main_bottom.animate().translationY(0);
                    }
                    super.onScrolled(recyclerView, dx, dy);
                }
            });
            DividerItemDecoration decoration = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
            recyclerView.addItemDecoration(decoration);
        }
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    protected void initData() {
        //DataNewsHelper dataNewsHelper = new DataNewsHelper();
        int sourceType = DataNewsHelper.getType(type,0);
        if(sourceType == 11){
            //数据源为RSS
            DataNewsHelper.parseRSS(tabTitle,src,type);
        }
        super.initData();
    }

    @Override
    protected void beforDestroy() {
        if(EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //swipeRefreshLayout.setRefreshing(true);
        //initData();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void NewsItemsEvent(NewsItemsEvent newsItemsEvent) {
        Log.i("EventBus", "NewsItemsEvent: ");
        if(tabTitle.equals(newsItemsEvent.getTabTitle())) {
            ArrayList<NewsItem> newsItems = newsItemsEvent.getNewsItem();
            adapter.update(newsItems);
            swipeRefreshLayout.setRefreshing(false);
        }
    }

}
