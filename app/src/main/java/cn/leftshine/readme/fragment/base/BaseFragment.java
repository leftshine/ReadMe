package cn.leftshine.readme.fragment.base;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import solid.ren.skinlibrary.base.SkinBaseFragment;

/**
 * Created by Administrator on 2018/10/19.
 */

public abstract class BaseFragment extends SkinBaseFragment {
    private View mContentView;
    private Context mContext;
    private ProgressDialog mProgressDialog;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mContentView = inflater.inflate(setLayoutResourceID(), container, false);//setContentView(inflater, container);
        mContext = getContext();
        mProgressDialog = new ProgressDialog(getMContext());
        mProgressDialog.setCanceledOnTouchOutside(false);
        init();
        initView();
        initData();
        return mContentView;
    }

    protected abstract int setLayoutResourceID();

    protected void initData() {

    }

    protected void init() {

    }

    protected void initView() {
    }

    protected  void beforDestroy(){

    }

    protected <T extends View> T $(int id) {
        return (T) mContentView.findViewById(id);
    }

    // protected abstract View setContentView(LayoutInflater inflater, ViewGroup container);

    protected View getContentView() {
        return mContentView;
    }

    public Context getMContext() {
        return mContext;
    }

    protected ProgressDialog getProgressDialog() {
        return mProgressDialog;
    }

}
