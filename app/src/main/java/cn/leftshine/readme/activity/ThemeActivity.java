package cn.leftshine.readme.activity;


import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import cn.leftshine.readme.R;
import cn.leftshine.readme.activity.base.BaseActivity;
import cn.leftshine.readme.adapter.ThemeGridViewAdpter;
import cn.leftshine.readme.beans.ThemeItem;
import cn.leftshine.readme.helper.SettingsHelper;
import cn.leftshine.readme.helper.ThemeInfoHelper;
import cn.leftshine.readme.listener.ThemeCacheRefreshListener;
import cn.leftshine.readme.listener.ThemeLoaderListener;
import solid.ren.skinlibrary.SkinConfig;
import solid.ren.skinlibrary.SkinLoaderListener;
import solid.ren.skinlibrary.loader.SkinManager;
import solid.ren.skinlibrary.utils.SkinFileUtils;

import static cn.leftshine.readme.helper.ThemeInfoHelper.refreshThemeCache;

public class ThemeActivity extends BaseActivity {

    private GridView gv_theme;
    private ThemeGridViewAdpter mAdapter = null;
    private ArrayList<ThemeItem> mData = null;
    private Context context;
    private SettingsHelper settingsHelper;
    private Snackbar snackbar;
    private Toolbar toolbar;
    private ProgressBar pb_theme_load;

    @Override
    protected void init() {
        context = this;
        settingsHelper = new SettingsHelper(context);
        super.init();
    }

    @Override
    protected int setLayoutResourceID() {
        return R.layout.activity_theme;
    }

    @Override
    protected void initView() {
        gv_theme = $(R.id.gv_theme);
        ViewCompat.setNestedScrollingEnabled(gv_theme,true);
        snackbar = Snackbar.make(gv_theme, R.string.tip_one_more_click_to_exit, Snackbar.LENGTH_LONG);
        Toolbar toolbar = $(R.id.toolbar);
        setSupportActionBar(toolbar);
        TextView tvSnackbarText = (TextView) snackbar.getView().findViewById(android.support.design.R.id.snackbar_text);
        pb_theme_load = $(R.id.pb_theme_load);

        //dynamicAddView(toolbar,"background",R.color.colorPrimary);
        dynamicAddView(toolbar,"background",R.color.colorPrimary);
        dynamicAddView(snackbar.getView(), "background", R.color.colorAccent);
        dynamicAddView(tvSnackbarText, "textColor", R.color.color_content_text);
    }

    @Override
    protected void initData() {
        final ThemeItem defaultTheme = new ThemeItem("default.skin", getResources().getString(R.string.title_theme_default), getResources().getColor(R.color.colorPrimary));
        mData = new ArrayList<ThemeItem>();
        mData.add(defaultTheme);
        //mData.add(new ThemeItem("",getResources().getString(R.string.title_theme_default),R.color.colorPrimary));
        /*mData.add(new ThemeItem("主题1",R.color.color_content_bg));
        mData.add(new ThemeItem("主题1",R.color.color_content_text));
        mData.add(new ThemeItem("主题1",R.color.color_bottom_navigation_bg));
        mData.add(new ThemeItem("主题1",R.color.color_bottom_navigation_bg));
        mData.add(new ThemeItem("主题1",R.color.color_bottom_navigation_bg));
        mData.add(new ThemeItem("主题1",R.color.color_bottom_navigation_bg));
        mData.add(new ThemeItem("主题1",R.color.color_bottom_navigation_bg));*/
        //String skinPkgPath = SkinFileUtils.getSkinDir(context);
        final File skinDir = new File(SkinFileUtils.getCacheDir(context), SkinConfig.SKIN_DIR_NAME);
        ThemeInfoHelper.getThemes(context, skinDir, new ThemeLoaderListener() {
            @Override
            public void onStart() {

            }

            @Override
            public void onSuccess(ArrayList<ThemeItem> items) {
                items.add(0, defaultTheme);
                mAdapter.update(items);
                pb_theme_load.animate().setDuration(500).alpha(0f).start();
            }

            @Override
            public void onFailed(String errMsg) {
                pb_theme_load.animate().setDuration(500).alpha(0f).start();
            }
        });

        mAdapter = new ThemeGridViewAdpter(mData, R.layout.layout_theme_item, context) {
            @Override
            public void bindView(ViewHolder holder, ThemeItem obj) {
                holder.setBackground(R.id.img_theme_item, obj.getColor());
                holder.setText(R.id.tv_theme_item, obj.getName());
            }
        };
        gv_theme.setAdapter(mAdapter);

        gv_theme.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ThemeItem item = mAdapter.getItem(position);
                final String skinFilename = item.getFilename();
                final String skinName = item.getName();
                if (skinFilename.equals(ThemeInfoHelper.DEFAULT_SKIN_FILE_NAME)) {
                    SkinManager.getInstance().restoreDefaultTheme();
                    settingsHelper.setLastThemeFilename(skinFilename);
                    settingsHelper.setNightmode(false);
                    showSnackbar(getString(R.string.tip_theme_default));
                    //Toast.makeText(getApplicationContext(), R.string.tip_theme_default, Toast.LENGTH_SHORT).show();
                } else {
                    if (skinFilename.equals("skin_black.skin")) {
                        settingsHelper.setNightmode(true);
                    } else {
                        settingsHelper.setNightmode(false);
                    }
                    SkinManager.getInstance().loadSkin(skinFilename,
                            new SkinLoaderListener() {
                                @Override
                                public void onStart() {
                                }

                                @Override
                                public void onSuccess() {
                                    //Toast.makeText(getApplicationContext(), getString(R.string.tip_theme_switch_success,skinName), Toast.LENGTH_SHORT).show();
                                    settingsHelper.setLastThemeFilename(skinFilename);
                                    showSnackbar(getString(R.string.tip_theme_switch_success, skinName));

                                }

                                @Override
                                public void onFailed(String errMsg) {
                                    //Toast.makeText(getApplicationContext(), getString(R.string.tip_theme_switch_fail, skinName), Toast.LENGTH_SHORT).show();
                                    showSnackbar(getString(R.string.tip_theme_switch_fail, skinName));
                                }

                                @Override
                                public void onProgress(int progress) {
                                    Log.i("Theme", "onProgress: " + progress);
                                }

                            }
                    );
                }

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_theme,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_theme_cache:
                refreshThemeCache(context, new ThemeCacheRefreshListener() {
                    @Override
                    public void onStart() {
                        pb_theme_load.animate().setDuration(100).alpha(1f).start();
                    }

                    @Override
                    public void onSuccess() {

                        initData();

                    }

                    @Override
                    public void onFailed(String errMsg) {

                    }
                });
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void beforDestroy() {

    }

    private void showSnackbar(String tip) {
        snackbar.setText(tip);
        snackbar.show();
    }


}
