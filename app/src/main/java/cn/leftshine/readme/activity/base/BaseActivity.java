package cn.leftshine.readme.activity.base;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import solid.ren.skinlibrary.base.SkinBaseActivity;

/**
 * Created by Administrator on 2018/10/18.
 */

public abstract class BaseActivity  extends SkinBaseActivity  {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
        setContentView(setLayoutResourceID());
        initView();
        initData();
    }

    @Override
    protected void onDestroy() {

        beforDestroy();
        super.onDestroy();
    }



    protected void init() {
    }

    protected abstract int setLayoutResourceID();

    protected abstract void initView();

    protected abstract void initData();

    protected abstract void beforDestroy();

    protected <T extends View> T $(int id) {
        return (T) super.findViewById(id);
    }


    protected void startActivityWithoutExtras(Class<?> clazz) {
        Intent intent = new Intent(this, clazz);
        startActivity(intent);
    }

    protected void startActivityWithExtras(Class<?> clazz, Bundle extras) {
        Intent intent = new Intent(this, clazz);
        intent.putExtras(extras);
        startActivity(intent);

    }



}
