package cn.leftshine.readme.listener;

import java.util.ArrayList;

import cn.leftshine.readme.beans.ThemeItem;

/**
 * Created by Administrator on 2018/10/29.
 */

public interface ThemeCacheRefreshListener {

    void onStart();

    void onSuccess();

    void onFailed(String errMsg);
}
