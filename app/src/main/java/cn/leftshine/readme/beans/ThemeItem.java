package cn.leftshine.readme.beans;

/**
 * Created by Administrator on 2018/10/26.
 */

public class ThemeItem {
    private String filename;
    private String name;
    private int color;

    public ThemeItem(String filename,String name, int color) {
        this.filename = filename;
        this.name = name;
        this.color = color;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }
}
