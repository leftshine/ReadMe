package cn.leftshine.readme.beans;

import java.io.Serializable;

/**
 * Created by Administrator on 2018/11/2.
 */

public class ImageSource implements Serializable {
    public String tabTitle;
    public int type;
    public String src;


    public ImageSource(String tabTitle, int type, String src) {
        this.tabTitle = tabTitle;
        this.type = type;
        this.src = src;
    }
}
